package ro.ionutscheianu.tema3.analyzer.bruteforce;

import ro.ionutscheianu.tema3.Graph;
import ro.ionutscheianu.tema3.analyzer.RouteAnalyzer;

public class BruteForceRunnable implements Runnable{

    private final Graph graph;

    public BruteForceRunnable(Graph graph) {
        this.graph = graph;
    }

    @Override
    public void run() {
        RouteAnalyzer bruteForceAnalyzer = new BruteForceAnalyzer(graph);
        bruteForceAnalyzer.calculateRoute(0);
        System.out.println("Brute force completed \n");
    }
}
